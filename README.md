# BME_Shield_Design
This is a design based on the BME 280 and probably used for the other BME180 etc.. This desgin contains the stl file, so each and everyone can easily 3D print the design. I developed the design using onshape tool. The link for the document is noted on contributing.md.

# Getting Started
- stl File so easily 3D printed
- after printing you can add any filter by wrapping a mask texture to the inner printed material.


# Prerequisites
Design [Tested]

# Contributing
Instructions coming up soon.

# License
This project is licensed under the MIT License - see the LICENSE.md file for details

# Acknowledgments
The design work perfectly under any scenario.

# Changelog
